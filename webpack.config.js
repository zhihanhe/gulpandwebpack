// V2
const webpack = require('webpack');
const path = require('path');
const fs = require('fs');

const fireMap = {};

const srcDir = './src/javascript/page/';

const fileList = fs.readdirSync(srcDir);

for(const name of fileList) {
    const fileName = name.split('.')[0];
    if(fileName) {
        fireMap[fileName] = srcDir + name;
    }
}

const conf = {
    entry : fireMap,

    output: {
        //文件输出由gulp指定
        //path: path.resolve(__dirname, 'dist'),
        filename: '[name].js',
        //文件的加载路径
        //publicPath: '',
    },

    module: {
        rules: [
            {
                test: /\.jsx?$/,
                loader: "babel-loader",
                options: {
                    presets: ['es2015', 'react']
                },
            }
        ]
    },

    externals: {
        'react': 'React',
        'react-dom': 'ReactDOM',
        'axios': 'axios',
        'antd': 'antd',
        'moment': 'moment',
        'Qs': 'Qs',
    },

    // 加载文件的路径配置
    resolve: {
        extensions: ['.js' ,'.jsx'],
        modules: ['node_modules'],
        alias: {
            //webrtcAdapter$: path.resolve(__dirname, 'websrc/javascripts/lib/adapter_no_edge.js'),
            // 'react': path.resolve(__dirname, '/resources/assets/lib/js/react-with-addons.js'),
            // 'react-dom': path.resolve(__dirname, '/resources/assets/lib/js/react-dom.js'),
        },
    },

    devtool: "source-map",
    //devtool: "inline-eval-cheap-source-map",

    target: 'web',

    //watch: true,

    plugins: [
      // new webpack.optimize.OccurenceOrderPlugin(),
      // new webpack.optimize.CaseSensitivePathsPlugin(),
    ],
};

module.exports = conf;