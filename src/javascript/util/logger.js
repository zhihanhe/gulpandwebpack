/*
 * Olive (Orange Labs Interactive Video Engine)
 *
 * (C) Copyright 2015 Orange labs.  All rights reserved.
 *
 */

function logger(...args) {
    console.log(args);
}

export default logger;
