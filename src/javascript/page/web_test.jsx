/*
 * Olive (Orange Labs Interactive Video Engine)
 *
 * (C) Copyright 2015 Orange labs.  All rights reserved.
 *
 */

import React from 'react';
import ReactDOM from 'react-dom';

import axios from 'axios';

import logger from '../util/logger';
import Test from '../component/test';

logger('hello', 'world4');

const div = document.createElement('div');
document.body.appendChild(div);


ReactDOM.render(<Test />, div);
