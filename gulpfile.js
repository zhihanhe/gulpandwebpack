const gulp = require('gulp');
const webpackStream = require('webpack-stream');
const webpack2 = require('webpack');

const uglify = require('gulp-uglify');

const cleanCSS = require('gulp-clean-css');

//引入gulp-load-plugins插件
/*
无需再一个个引入要使用的插件，可以直接在使用时用plugins.pluginName来调用
gulp-load-plugins插件在我们需要用到某个插件的时候，才去加载那个插件，
并不是一开始就全部加载进来。因为gulp-load-plugins是依赖package.json文件来加载插件的，
所以请确保你需要的插件已经加入package.json文件并已经安装完毕。
*/
const plugins = require('gulp-load-plugins')();

// 要处理的
const jsSrcDir = ['src/javascript/**'];

const jsDistDir = 'dist/js/page/';

// 要压缩的文件
const jsLibs = ['src/vendor/js/react-with-addons.js',
                'src/vendor/js/react-dom.js',
                'src/vendor/js/qs.js',
                'src/vendor/js/axios.js',
                'src/vendor/js/moment.js',
                'src/vendor/js/antd.js'];

// 压缩后文件的名字
const jsLibName = 'vendor.min.js';

// 压缩后css的文件路径
const jsLibDist = 'dist/js/lib';

// sass源文件的地址
const sassSrcDir = 'src/sass/**';

// css目标文件的地址
const cssDistDir = 'dist/css/';

// js代码检测的地址
const lintSrc = ['src/javascript/**'];


/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

gulp.task('sass',() =>{
    return gulp.src(sassSrcDir)
        .pipe(plugins.sass())
        .pipe(gulp.dest(cssDistDir))
        //gulp之自动刷新gulp-livereload
        .pipe(plugins.livereload());
});

gulp.task('minify-css', ['sass'], () =>{
    // return gulp.src('public/css/web_?*.css')
    return gulp.src(cssDistDir)
        .pipe(cleanCSS())
        .pipe(gulp.dest(cssDistDir));
});

//代码格式检测
gulp.task('lint', () => {
    return gulp.src(lintSrc)
        // eslint() attaches the lint output to the "eslint" property
        // of the file object so it can be used by other modules.
        .pipe(plugins.eslint({
            rules: {
                //空格4个
                'indent': ['error', 4],
                'no-underscore-dangle' : ['error', { 'allowAfterThis': true, 'allowAfterSuper': true }],
                //"prefer-stateless-function": [2, { "ignorePureComponents": true }],
                "react/jsx-no-bind": [2, {
                    "ignoreRefs": true,
                    "allowArrowFunctions": true,
                    "allowBind": true
                }]
            },
            globals: [
                'webrtcAdapter',
            ],
            baseConfig : {
                "extends": "eslint-config-airbnb"
            },
            envs: [
                'browser', 'es6'
            ],
        }))
        // eslint.format() outputs the lint results to the console.
        // Alternatively use eslint.formatEach() (see Docs).
        .pipe(plugins.eslint.format())
        // To have the process exit with an error code (1) on
        // lint error, return the stream and pipe to failAfterError last.
        .pipe(plugins.eslint.failAfterError());
});

//合并通用的库
gulp.task('scripts-vendor', function(callback) {
  return gulp.src(jsLibs)
      .pipe(plugins.concat(jsLibName))
      .pipe(gulp.dest(jsLibDist))
      // .pipe(plugins.notify({ message: '合并lib库成功' }));
});

gulp.task('scripts-vendor-uglify', ['scripts-vendor'], function(callback) {
  return gulp.src([jsLibDist])
      .pipe(uglify())
      .pipe(gulp.dest(jsLibDist))
      // .pipe(plugins.notify({ message: '合并lib库成功' }));
});

gulp.task( 'combine', () => {
    return gulp.src(jsSrcDir)
        .pipe(webpackStream(require('./webpack.config.js'),webpack2))
        .pipe(gulp.dest(jsDistDir))
        .pipe(plugins.notify({ message: '转换代码完成' }))
        //.pipe(plugins.livereload());
});

gulp.task( 'combine-uglify', ['combine'], () => {
    // 压缩部分文件
    // return gulp.src('public/js/page/web_?*.js')
    return gulp.src(jsDistDir)
        .pipe(plugins.stripDebug())
        .pipe(uglify())
        .pipe(gulp.dest(jsDistDir));
});

gulp.task('watch', function() {
    gulp.watch(jsSrcDir,['lint'])
    gulp.watch(jsSrcDir,['combine'])
    gulp.watch(sassSrcDir,['sass'])
});

gulp.task('default', ['scripts-vendor','watch','sass','combine'], () => {

});

gulp.task('package', ['scripts-vendor-uglify', 'combine-uglify', 'minify-css'], () => {
    
});
